# Showcase - Smarte Suche

Diese Anwendung dient dazu semantische Ähnlichkeit von Wörtern darzustellen.

![Beispiel](example.png)

Zu einem eingegebenen Wort werden semantisch ähnliche Wörter gesucht und deren ähnlichkeit grafisch in einem 2D-Plot
dargestellt. Dazu werden im Hintergrund vortrainierte [Word Embeddings](https://de.wikipedia.org/wiki/Worteinbettung)
verwendet.
Dies soll veranschaulichen, wie Sprachmodelle die Bedeutung von Wörtern erfassen.

## Installation und Start der Anwendung

Zuerst müssen die benötigten Pakete installiert werden.
Dies geschieht mittels des folgenden Befehl:

```shell
pip install -r requirements.txt
```

Um die Anwendung zu starten, muss das Python-Script über den folgenden Befehl gestartet werden:

```shell
python main.py
```

Die Anwendung wird beendet, indem das Anwedungsfenster geschlossen wird.

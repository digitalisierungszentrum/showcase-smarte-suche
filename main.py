import gensim
import matplotlib.pyplot as plt
from gensim.models import Word2Vec
from matplotlib.widgets import TextBox, Slider
from sklearn.decomposition import PCA


def submit(search_word, n_words=5):
    """
    Searches for similar words and plots their PCA representation.

    :param search_word: The word passed by the edit field
    :param n_words: The number of similar words to search
    """
    # Clear plot each time and remove grid
    axs[0].clear()
    axs[0].axis('off')

    if search_word not in model:
        axs[0].text(x=0, y=0, s=f"Das gesuchte Wort '{search_word}' wurde nicht gefunden.")
        plt.draw()
        return
    else:
        print(f"Suche nach {n_words} ähnlichen Wörtern zu '{search_word}'...")

    # Get next five words
    similar_words = model.most_similar(search_word, topn=n_words)

    # Create a list of all words to be processed
    word_list = [search_word] + [sw[0] for sw in similar_words]

    # Get the vectors of each word
    vectors = [model[word] for word in word_list]

    # Apply a PCA with two components
    pca = PCA(n_components=2, whiten=True)
    vectors2d = pca.fit(vectors).transform(vectors)

    # Remember positions of search word to draw arrows
    search_word_x = vectors2d[0][0]
    search_word_y = vectors2d[0][1]
    comparison_index = 0
    for point, word in zip(vectors2d, word_list):
        # Plot circles at the PCA coordinates
        axs[0].scatter(point[0], point[1], c='r')
        # Plot the name of the current word
        axs[0].annotate(
            word,
            xy=(point[0], point[1])
        )

        # If it's not the first word, draw an arrow from the first word to the current word
        if comparison_index > 0:
            comparison_x = vectors2d[comparison_index][0]
            comparison_y = vectors2d[comparison_index][1]
            axs[0].arrow(
                search_word_x, search_word_y, comparison_x - search_word_x, comparison_y - search_word_y,
                shape='full',
                lw=0.1,
                edgecolor='#bbbbbb',
                facecolor='#bbbbbb',
                length_includes_head=True,
                head_width=0.08,
                width=0.01
            )
        comparison_index = comparison_index + 1
    plt.draw()


# Load the model and set up plot
model = gensim.models.KeyedVectors.load_word2vec_format("german.model", binary=True)
fig, axs = plt.subplots(4, gridspec_kw={'height_ratios': [10, 1, 2, 1]})

plt.get_current_fig_manager().set_window_title('Wortassoziationen')
axs[0].axis('off')


def set_slider(s, val):
    s.val = round(val)
    s.valtext.set_text(s.valfmt % s.val)


slider = Slider(axs[3], 'Anzahl ähnlicher Wörter: ', 3, 10, valinit=3, valfmt="%i")
slider.on_changed(lambda val: set_slider(slider, val))
slider_label = axs[3].get_children()[4]
slider_label.set_position([0., 1.6])
slider_label.set_verticalalignment('top')
slider_label.set_horizontalalignment('left')

text_box = TextBox(axs[1], "Wort: ")
text_box.on_submit(lambda word: submit(word, round(slider.val)))

axs[2].text(x=0, y=0, s="Geben Sie in Wort ein und bestätigen mit Enter.\n"
                        "Umlaute sollten als 'ae', 'oe' und 'ue' geschrieben werden.\n")
axs[2].axis('off')

plt.show()
